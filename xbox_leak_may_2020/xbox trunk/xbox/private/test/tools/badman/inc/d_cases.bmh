/*++

Copyright (c) 1992-2000  Microsoft Corporation

Module Name:

    d_cases.bmh

Abstract:

    This module contains the case names and comments for the types beginning
    with 'D'. It also contains case names for funtions beginning with 'LPD'.
    For more information, please refer to BadMan.Doc.

Author:

    John Miller (johnmil) 02-Feb-1992

Environment:

    XBox

Revision History:

    01-Apr-2000     schanbai

        Ported to XBox and removed not needed cases

--*/


//
// DWORD type
//

        // Valid combination of valid flags for LocalAlloc
#define DWORD_VALID_LOCALALLOC_FLAGS                    1

        // Combination of valid flags and spurious bits for LocalAlloc
#define DWORD_VALID_AND_SPURIOUS_LOCALALLOC_FLAGS       3

        // Spurious bits only for LocalAlloc
#define DWORD_SPURIOUS_LOCALALLOC_FLAGS                 4

        // Valid combination of valid flags for GlobalAlloc
#define DWORD_VALID_GLOBALALLOC_FLAGS                   5

        // Combination of Valid flags and spurious bits for GlobalAlloc
#define DWORD_VALID_AND_SPURIOUS_GLOBALALLOC_FLAGS      7

        // Spurious bits only for GlobalAlloc
#define DWORD_SPURIOUS_GLOBALALLOC_FLAGS                8

        // Valid combination of valid flags for LocalReAlloc
#define DWORD_VALID_LOCALREALLOC_FLAGS                  9

        // Invalid combination of valid flags for LocalReAlloc
#define DWORD_INVALID_LOCALREALLOC_FLAGS                10

        // Combination of valid flags and spurious bits for LocalReAlloc
#define DWORD_VALID_AND_SPURIOUS_LOCALREALLOC_FLAGS     11

        // Spurious bits only for LocalReAlloc
#define DWORD_SPURIOUS_LOCALREALLOC_FLAGS               12

        // Valid combination of valid flags for GlobalReAlloc
#define DWORD_VALID_GLOBALREALLOC_FLAGS                 13

        // Invalid combination of valid flags for GlobalReAlloc
#define DWORD_INVALID_GLOBALREALLOC_FLAGS               14

        // Combination of valid flags and spurious bits for GlobalReAlloc
#define DWORD_VALID_AND_SPURIOUS_GLOBALREALLOC_FLAGS    15

        // Spurious bits only for GlobalReAlloc
#define DWORD_SPURIOUS_GLOBALREALLOC_FLAGS              16

        // The number 1024
#define DWORD_ONE_K                                     17

        // The number 1024*1024
#define DWORD_ONE_MEG                                   18

        // Three gigabytes
#define DWORD_THREE_GIG                                 19

        // The number 0
#define DWORD_ZERO                                      20

        // The number 32*1024*1024
#define DWORD_32MB                                      21

        // The number -1
#define DWORD_MINUS_ONE                                 25

        // GENERIC_ALL access mask
#define DWORD_ACCESSMASK_GENALL                         27

        // MAX_ULONG
#define DWORD_MAXULONG                                  28

        // Tnumber 1
#define DWORD_ONE                                       30

        // MEM_COMMIT flag for VirtualAlloc
#define DWORD_VMEM_COMMIT_FLAG                          31

        // MEM_RESERVE flag for VirtualAlloc
#define DWORD_VMEM_RESERVE_FLAG                         32

        // PAGE_READWRITE for VirtualAlloc
#define DWORD_VMEM_READWRITE_FLAG                       33

        // Invalid value for dwProtect for VirtualAlloc
#define DWORD_INVALID_VMEM_PROT                         34

        // MEM_DECOMMIT flag for VirtualFree
#define DWORD_VMEM_DECOMMIT_FLAG                        35

        // MEM_RELEASE flag for VirtualFree
#define DWORD_VMEM_RELEASE_FLAG                         36

        // Invalid value for dwFreeType for VirtualFree
#define DWORD_VMEM_INVAL_FREETYPE                       37

        // sizeof(MEMORY_BASIC_INFORMATION)
#define DWORD_VMEM_INFOSIZE                             38

        // Heap serialize flag is set
#define DWORD_HEAP_SERIALIZE_FLAG                       39

        // bitwise complement of HEAP_SERIALIZE
#define DWORD_HEAP_BADSERFLAG                           40

        // Bitwise compliment of the reserve and commit flags
#define DWORD_VMEM_BADALLOCFLAG                         41

        // ALL_ACCESS Access mask
#define DWORD_PROCESS_ALL_ACCESS                        42

        // Bad access mask for process
#define DWORD_PROCESS_BAD_ACCESS                        43

        // DUP_HANDLE Access mask
#define DWORD_PROCESS_DUP_HANDLE                        44

        // QUERY_INFORMATION Access mask
#define DWORD_PROCESS_QUERY_INFORMATION                 45

        // SET_INFORMATION Access mask
#define DWORD_PROCESS_SET_INFORMATION                   46

        // SYNCHRONIZE Access mask
#define DWORD_PROCESS_SYNCHRONIZE                       47

        // VM_READ Access mask
#define DWORD_PROCESS_VM_READ                           48

        // VM_WRITE Access mask
#define DWORD_PROCESS_VM_WRITE                          49

        // ALL_ACCESS Access mask
#define DWORD_THREAD_ALL_ACCESS                         50

        // Bad access mask for thread
#define DWORD_THREAD_BAD_ACCESS                         51

        // GET_CONTEXT Access mask
#define DWORD_THREAD_GET_CONTEXT                        52

        // QUERY_INFORMATION Access mask
#define DWORD_THREAD_QUERY_INFORMATION                  53

        // SET_INFORMATION Access mask
#define DWORD_THREAD_SET_INFORMATION                    54

        // SET_CONTEXT Access mask
#define DWORD_THREAD_SET_CONTEXT                        55

        // SUSPEND_RESUME Access mask
#define DWORD_THREAD_SUSPEND_RESUME                     56

        // SYNCHRONIZE Access mask
#define DWORD_THREAD_SYNCHRONIZE                        57

        // Max length of TokenInformation Class
#define DWORD_TOKEN_INFO_LENGTH                         58

        // Length of TOKEN_PRIVILEGES
#define DWORD_TOKEN_PRIV_LENGTH                         59

        // Length of TOKEN_GROUPS
#define DWORD_TOKEN_GROUPS_LENGTH                       60

        // The GMEM_MODIFY flag for Global memory
#define DWORD_GMEM_MODIFY_FLAG                          61

        // The LMEM_MODIFY flag for Local memory
#define DWORD_LMEM_MODIFY_FLAG                          62

        // Create process with normal priority class
#define DWORD_NORMAL_PRIORITY_CLASS                     63

        // create process with idle priority class
#define DWORD_IDLE_PRIORITY_CLASS                       64

        // create process with high priority class
#define DWORD_HIGH_PRIORITY_CLASS                       65

        // create a suspended process
#define DWORD_CREATE_SUSPENDED                          66

        // create a detached process
#define DWORD_CREATE_DETACHED_PROCESS                   67

        // create a new console for the child process
#define DWORD_CREATE_NEW_CONSOLE                        68

        // create process with new console and detached
#define DWORD_CREATE_DETACHED_AND_NEW_CONSOLE           69

        // create process with multiple priority classes
#define DWORD_CREATE_MULTIPLE_PRIORITY_CLASSES          70

        // ID of currently running process
#define DWORD_CURRENT_PROCESS_ID                        71

        // Normal Thread creation flags (0)
#define DWORD_NORMAL_THREAD_FLAGS                       72

        // Create thread suspended
#define DWORD_THREAD_CREATE_SUSPENDED                   73

        // Valid Thread Local Storage (TLS) index
#define DWORD_VALID_TLS_SLOT                            74

        // Valid Thread Local Storage (TLS) index that has been freed
#define DWORD_FREED_TLS_SLOT                            75

        // desired access mask
#define DWORD_ACCESSMASK_DESIRED                        84

        // granted access mask
#define DWORD_ACCESSMASK_GRANTED                        85

        // generic read access mask
#define DWORD_ACCESSMASK_GENREAD                        86

        // length of a security descriptor structure
#define DWORD_SECURITY_DESCRIPTOR_LEN                   87

        // Valid generic read and write access
#define DWORD_GENERIC_READ_AND_WRITE                    88

        // Invalid access value
#define DWORD_GENERIC_INVALID_VALUE                     89

        // Valid read and write sharing mode
#define DWORD_FILE_SHARE_READ_AND_WRITE                 90

        // Invalid sharing mode
#define DWORD_FILE_SHARE_INVALID_VALUE                  91

        // Valid creation disposition, create file always
#define DWORD_DISPOSITION_CREATE_FILE_ALWAYS            92

        // Invalid creation disposition value
#define DWORD_DISPOSITION_INVALID_VALUE                 93

        // Valid file flags and attributes, normal attribs
#define DWORD_FILE_FLAGS_NORMAL                         94

        // Invalid file flags and attributes value
#define DWORD_FILE_FLAGS_INVALID_VALUE                  95

        // valid read/write buffer size
#define DWORD_READWRITE_VALID_BUFFER_SIZE               96

        // invalid read/write buffer size
#define DWORD_READWRITE_INVALID_BUFFER_SIZE             97

        // move method from beginning of file
#define DWORD_MOVE_METHOD_FILE_BEGIN                    98

        // invalid move method value
#define DWORD_MOVE_METHOD_INVALID_VALUE                 99

        // length of buffer for logical drive string
#define DWORD_LOGICAL_DRIVE_STRING_BUFFER_LENGTH        100

        // valid path buffer length
#define DWORD_VALID_PATH_BUFFER_LENGTH                  101

        // maximum dword value
#define DWORD_MAX                                       102

        // length of volume name buffer
#define DWORD_VOLUME_NAME_BUFFER_LENGTH                 103

        // length of file system name buffer
#define DWORD_FILE_SYSTEM_NAME_BUFFER_LENGTH            104

        // valid file attributes
#define DWORD_NORMAL_FILE_ATTRIBUTES                    105

        // invalid file attributes value
#define DWORD_INVALID_FILE_ATTRIBUTES                   106

        // valid change notify filter value
#define DWORD_VALID_CHANGE_NOTIFY_FILTER                121

        // invalid change notify filter value
#define DWORD_INVALID_CHANGE_NOTIFY_FILTER              122

        // invalid flags passed to VerFindFile
#define DWORD_VER_INVALID_FIND_FLAGS                    123

        // invalid flags passed to VerInstallFile
#define DWORD_VER_INVALID_INST_FLAGS                    124

        // valid length of versioning info
#define DWORD_VER_VALID_INFO_LENGTH                     125

        // ms id for us english
#define DWORD_VER_LANG_US_ENGLISH                       126

        // undefined ms language id
#define DWORD_VER_LANG_UNDEFINED                        127

        // size of a valid language buffer
#define DWORD_VER_LANGUAGE_BUFFER_SIZE                  128

        // 12
#define DWORD_12                                        129

        // 16
#define DWORD_16                                        130

        // 50
#define DWORD_50                                        131

        // 65535 (64k - 1 really)
#define DWORD_64K                                       132

        // 65537
#define DWORD_65537                                     133

        // 8
#define DWORD_EIGHT                                     138

        // 4
#define DWORD_FOUR                                      139

        // GENERIC_ALL
#define DWORD_GENERIC_ALL                               140

        // GENERIC_EXECUTE
#define DWORD_GENERIC_EXECUTE                           141

        // GENERIC_READ
#define DWORD_GENERIC_READ                              142

        // GENERIC_WRITE
#define DWORD_GENERIC_WRITE                             143

        // SECURITY_DESCRIPTOR_MIN_LENGTH
#define DWORD_MIN_SD_LENGTH                             144

        // a random 32 bit value
#define DWORD_RANDOM                                    145

        // READ_CONTROL
#define DWORD_READ_CONTROL                              146

        // SYNCHRONIZE
#define DWORD_SYNCHRONIZE                               147

        // 2
#define DWORD_TWO                                       148

        // 13
#define DWORD_13                                        202

        // Valid Direct3D execution flags
#define DWORD_D3DEXECUTE_CLIPPED                        248

        // Valid Direct3D execution flags
#define DWORD_D3DEXECUTE_UNCLIPPED                      249

        // Invalid DirectX D3D execution flags
#define DWORD_D3DEXECUTE_BOTH                           250

        // Invalid Direct3D execution flags
#define DWORD_D3DEXECUTE_NONE                           251

        // Current thread ID
#define DWORD_THREADID                                  257

        // Valid read/write/delete sharing mode
#define DWORD_FILE_SHARE_READ_WRITE_DELETE              258


//
// LPDEVMODEA type
//

        // NULL
#define LPDEVMODEA_NULL         1

        // Valid but empty
#define LPDEVMODEA_VALID        2


//
// LPDEVMODEW type
//

        // NULL
#define LPDEVMODEW_NULL         1

        // Valid but empty
#define LPDEVMODEW_VALID        2


//
// LPDWORD type
//

        // A null buffer
#define LPDWORD_NULL                7

        // A buffer to store return length info
#define LPDWORD_BUFFER              8

        // A pointer value of -1
#define LPDWORD_MINUS_ONE           13

        // A random pointer value
#define LPDWORD_RANDOM_POINTER      14

        // a pointer to a DWORD sized memory block
#define LPDWORD_DWORD_SIZED_BLOCK   15

        // Valid dword address
#define LPDWORD_VALID_ADDRESS       17

        // Points to 65536
#define LPDWORD_64K                 18

        // Points to a 2
#define LPDWORD_TWO                 19

        // An invalid pointer
#define LPDWORD_INVALID             20

        // Points to a 0
#define LPDWORD_ZERO                21

        // -> 10
#define LPDWORD_10                  22

        // -> 50
#define LPDWORD_50                  23

        // -> 65537
#define LPDWORD_65537               25

        // -> 4
#define LPDWORD_FOUR                26

        // An un-aligned pointer
#define LPDWORD_MALALIGNED          27

        // (4,5,4)
#define	LPDWORD_454                 31


//
// LPDIRECT3DDEVICE type
//

        // Valid device
#define LPDIRECT3DDEVICE_VALID              1

        // NULL pointer
#define LPDIRECT3DDEVICE_NULL               2

        // 0xDEADBEEF pointer
#define LPDIRECT3DDEVICE_DEADBEEF           3

        // Random pointer
#define LPDIRECT3DDEVICE_RANDOM             4


//
// LPDIRECT3DEXECUTEBUFFER type
//
 
        // Valid execute buffer
#define LPDIRECT3DEXECUTEBUFFER_VALID       1

        // NULL pointer
#define LPDIRECT3DEXECUTEBUFFER_NULL        2

        // DEADBEEF pointer
#define LPDIRECT3DEXECUTEBUFFER_DEADBEEF    3

        // Random pointer
#define LPDIRECT3DEXECUTEBUFFER_RANDOM      4 

        // Locked buffer
#define LPDIRECT3DEXECUTEBUFFER_LOCKED      5


//
// LPDIRECT3DVIEWPORT type
//

        // Valid viewport
#define LPDIRECT3DVIEWPORT_VALID            1

        // NULL pointer
#define LPDIRECT3DVIEWPORT_NULL             2

        // DEADBEEF pointer
#define LPDIRECT3DVIEWPORT_DEADBEEF         3

        // Random pointer
#define LPDIRECT3DVIEWPORT_RANDOM           4
