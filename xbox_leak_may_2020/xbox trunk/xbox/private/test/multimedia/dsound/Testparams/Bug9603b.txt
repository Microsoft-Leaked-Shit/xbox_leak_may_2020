[TestList]
dmtest1                         ;DMusic tests
;dsbvt                           ;dsound bvt
;xmobvt                          ;xmo bvt
;wmabvt                          ;wma codec bvt
;dsreg                           ;dsound regression (until we have reg tests)
xmoreg                          ;xmo regression (until we have reg tests)
;wmareg              	        ;the wma regression
;ds3d                            ; dsound 3d tests
;DirectSoundCreate               ; direct sound create api tests
;DirectSoundCreateBuffer         ; direct sound create buffer tests
;DirectSoundCreateStream         ; direct sound create stream tests

[wmabvt]
ThreadStackSize=65536

[wmareg]
ThreadStackSize=65536


[DMTest1]
ThreadStackSize=0x10000

Wait=1
BVT=0
Valid=0
Invalid=0
Fatal=0
Perf=0
LogToScreen=0
WaitAtTestEnd=0
SkipUserInput=1
InitPerformance = Once
LocalLogLevel=1
DoWorkFrequency=30
SuppressMemInfo = 1

[DMTest1_TestAdd]
Performance8_CreateStandardAudioPath_BVT