[TestList]
dmtest1                           ;DMusic tests

[DMTest1]

ThreadStackSize=0x10000

Wait          =1
BVT           =0
Valid         =0
Invalid       = 0
Fatal         =0
Perf          =0
WaitAtTestEnd =1
InitPerformance = Once
LocalLogLevel =2
SkipUserInput = 0
Stress      = 1
LogToScreen = 1


MaxTestAdd = 40

 
TestAdd0001 =DLS_TestSegment(0200_ChanPressToFc_00000)
TestAdd0002 =DLS_TestSegment(0200_ChanPressToFc_12800)
TestAdd0003 =DLS_TestSegment(0200_ChanPressToFc_-12800)
TestAdd0004 =DLS_TestSegment(0210_ChanPressToGain_00db)
TestAdd0005 =DLS_TestSegment(0211_ChanPressToGain_12db)
TestAdd0006 =DLS_TestSegment(0220_ChanPressToPitch_-06)
TestAdd0007 =DLS_TestSegment(0221_ChanPressToPitch_-12)
TestAdd0008 =DLS_TestSegment(0222_ChanPressToPitch_00)
TestAdd0009 =DLS_TestSegment(0223_ChanPressToPitch_06)
TestAdd0010 =DLS_TestSegment(0224_ChanPressToPitch_12)
TestAdd0011 =DLS_TestSegment(0230_Delay00s)
TestAdd0012 =DLS_TestSegment(0231_Delay01s)
TestAdd0013 =DLS_TestSegment(0232_Delay05s)
TestAdd0014 =DLS_TestSegment(0233_Delay10s)
TestAdd0015 =DLS_TestSegment(0240_Frequency_01Hz)
TestAdd0016 =DLS_TestSegment(0241_Frequency_05Hz)
TestAdd0017 =DLS_TestSegment(0242_Frequency_0p1Hz)
TestAdd0018 =DLS_TestSegment(0242_Frequency_10Hz)
TestAdd0019 =DLS_TestSegment(0250_MWToPitch_-12)
TestAdd0020 =DLS_TestSegment(0251_MWToPitch_12)
TestAdd0021 =DLS_TestSegment(0260_MWToVolume_12)
TestAdd0022 =DLS_TestSegment(0270_PitchRange_-01)
TestAdd0023 =DLS_TestSegment(0271_PitchRange_-12)
TestAdd0024 =DLS_TestSegment(0272_PitchRange_00)
TestAdd0025 =DLS_TestSegment(0273_PitchRange_01)
TestAdd0026 =DLS_TestSegment(0274_PitchRange_12)
TestAdd0027 =DLS_TestSegment(0280_VolRange_00db)
TestAdd0028 =DLS_TestSegment(0281_VolRange_01db)
TestAdd0029 =DLS_TestSegment(0282_VolRange_05db)
TestAdd0030 =DLS_TestSegment(0283_VolRange_12db)
