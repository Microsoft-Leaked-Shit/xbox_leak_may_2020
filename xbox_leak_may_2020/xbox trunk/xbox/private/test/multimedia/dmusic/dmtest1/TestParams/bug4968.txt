[TestList]
dmtest1                           ;DMusic tests

[DMTest1]

ThreadStackSize=0x10000

Wait  =1
BVT   =0
Valid =0
Invalid = 0
Fatal =0
Perf  =0
PerfWait  =30
Stress = 1
InitPerformance = Once
LocalLogLevel        =2
DoWorkFrequency =30
WaitAtTestEnd =1
WaitBetweenTests=5

MaxTestAdd=70

;TestAdd0001=Performance8_PlaySegmentEx_Valid_DelayDoWork(Timing,500)
TestAdd0001=Script_TransSimple_PlayIntro(Startup)
;TestAdd0002=Script_TransSimple_PlayIntro(SameSeg)
;TestAdd0003=Script_TransSimple_PlayIntro(DiffSeg)
;TestAdd0004=Script_TransSimple_PlayFill (Startup)
;TestAdd0005=Script_TransSimple_PlayFill (SameSeg)
;TestAdd0006=Script_TransSimple_PlayFill (DiffSeg)
;TestAdd0007=Script_TransSimple_PlayBreak(Startup)
;TestAdd0008=Script_TransSimple_PlayBreak(SameSeg)
;TestAdd0009=Script_TransSimple_PlayBreak(DiffSeg)
;TestAdd0010=Script_TransSimple_PlayEnd  (Startup)
;TestAdd0011=Script_TransSimple_PlayEnd  (SameSeg)
;TestAdd0012=Script_TransSimple_PlayEnd  (DiffSeg)
