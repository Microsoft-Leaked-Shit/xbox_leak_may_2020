[TestList]
dmtest1                           ;DMusic tests

[DMTest1]

ThreadStackSize=0x10000

Wait          =1
BVT           =0
Valid         =0
Invalid       = 0
Fatal         =0
Perf          =0
WaitAtTestEnd =1
InitPerformance = Once
LocalLogLevel = 2
SkipUserInput = 1
Stress      = 1
LogToScreen = 1
SuppressMemInfo = 1
DefaultMedia = "t:\DMTest1\wav\streaming10s.wav"

MaxTestAdd = 700

TestAdd0000 = AudioPath_SetPitch_Valid_Ramp_1000ms(listen)
