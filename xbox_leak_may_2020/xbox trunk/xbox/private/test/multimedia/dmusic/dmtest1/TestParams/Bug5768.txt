[TestList]
dmtest1                           ;DMusic tests

[DMTest1]

ThreadStackSize=0x10000

Wait          =1
BVT           =0
Valid         =0
Invalid       = 0
Fatal         =0
Perf          =0
WaitAtTestEnd =1
InitPerformance = Once
LocalLogLevel =2
SkipUserInput = 0
Stress      = 1
LogToScreen = 1
SupressMemInfo = 1


MaxTestAdd = 70


TestAdd0039 =DLS_TestSegment(0050_Pan_Left_50)
TestAdd0040 =DLS_TestSegment(0051_Pan_Left_25)
TestAdd0041 =DLS_TestSegment(0052_Pan_Mid)
TestAdd0042 =DLS_TestSegment(0053_Pan_Right_25)
TestAdd0043 =DLS_TestSegment(0054_Pan_Right_50)