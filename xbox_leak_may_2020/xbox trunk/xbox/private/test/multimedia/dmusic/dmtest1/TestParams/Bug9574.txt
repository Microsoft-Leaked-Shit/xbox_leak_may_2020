[TestList]
dmtest1                           ;DMusic tests

[DMTest1]

ThreadStackSize=0x10000

Wait=1
BVT=0
Valid=0
Invalid=0
Fatal=0
Perf=0
LogToScreen=1
WaitAtTestEnd=0
SkipUserInput=1
InitPerformance = Once
LocalLogLevel=2
SuppressMemInfo = 1
Stress = 1
DoWorkLocalThread = 1
DoWorkFrequency=500

[DMTest1_TestAdd]
ADPCM_OneShot_11_1

;1000 = no, after at least 3 minutes.
;500 = 300 seconds.