[TestList]
dmtest1                           ;DMusic tests

[DMTest1]

ThreadStackSize=0x10000

Wait  =1
WaitBetweenTests = 1
BVT   =0
Valid =0
Invalid = 0
Fatal =0
Perf  =0
Stress =1
WaitAtTestEnd   =0 
SuppressMemInfo = 1
InitPerformance = Once
LocalLogLevel        =2
SkipUserInput=1

[DMTest1_TestAdd]
Performance8_PlaySegmentEx_ClockTime_Looping_SEGMENT_0_3_1r
;Performance8_PlaySegmentEx_ClockTime_Looping_SEGMENT_0_3_2r
;Performance8_PlaySegmentEx_ClockTime_Looping_SEGMENT_0_6_1r
;Performance8_PlaySegmentEx_ClockTime_Looping_SEGMENT_0_6_2r
;Performance8_PlaySegmentEx_ClockTime_Looping_SEGMENT_1_5_1r
;Performance8_PlaySegmentEx_ClockTime_Looping_SEGMENT_1_5_2r
;Performance8_PlaySegmentEx_ClockTime_Looping_SEGMENT_3_6_1r
;Performance8_PlaySegmentEx_ClockTime_Looping_SEGMENT_3_6_2r
;Performance8_PlaySegmentEx_ClockTime_Looping_WAVE_0_3_1r
;Performance8_PlaySegmentEx_ClockTime_Looping_WAVE_0_6_1r 
;Performance8_PlaySegmentEx_ClockTime_Looping_WAVE_1_5_1r 
;Performance8_PlaySegmentEx_ClockTime_Looping_WAVE_3_6_1r 
