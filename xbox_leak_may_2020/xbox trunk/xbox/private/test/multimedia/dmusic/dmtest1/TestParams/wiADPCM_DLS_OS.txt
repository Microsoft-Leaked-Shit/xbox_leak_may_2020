[TestList]
dmtest1                           ;DMusic tests

[DMTest1]

ThreadStackSize=0x10000

Wait          =1
BVT           =0
Valid         =0
Invalid       = 0
Fatal         =0
Perf          =0
WaitAtTestEnd =1
InitPerformance = Once
LocalLogLevel =2
SkipUserInput = 0
Stress      = 0
LogToScreen = 1


MaxTestAdd = 100


TestAdd0034 = ADPCM_DLS_OneShot_11_1
TestAdd0035 = ADPCM_DLS_OneShot_22_1
TestAdd0036 = ADPCM_DLS_OneShot_44_1
