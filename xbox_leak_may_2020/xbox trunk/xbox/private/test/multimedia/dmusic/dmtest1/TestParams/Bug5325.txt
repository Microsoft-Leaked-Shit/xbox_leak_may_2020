[TestList]
dmtest1                           ;DMusic tests

[DMTest1]

ThreadStackSize=0x10000

Wait  =1
BVT   =0
Valid =0
Invalid = 0
Fatal =0
Perf  =0
PerfWait  =30
Stress = 1
InitPerformance = Once
LocalLogLevel        =2
DoWorkFrequency =30
WaitAtTestEnd =1

MaxTestAdd=70

;TestAdd0001=Loader_SetSearchDirectory_BVT
;TestAdd0002=Loader_ReleaseObjectByUnknown_BVT
TestAdd0003=Performance8_AddNotificationType_BVT
