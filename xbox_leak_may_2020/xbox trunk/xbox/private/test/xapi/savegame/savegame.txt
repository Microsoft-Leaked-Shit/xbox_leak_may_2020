these are the apis we have so far, and test cases

XCreateSaveGame()
        - test with 0 saved games
        - 1 saved game
        - until the disk runs out of space, and then some
        - bad params where they make sense
        - lpRootPathName 0...N long w/ UNICODE chars
        - lpSaveGameName 0...N long w/ UNICODE chars
        - lpPathBuffer normal, NULL, too short

XDeleteSaveGame()
        - test with 0 saved games
        - 1 saved game
        - a disk full of saved games (thousands!)
        - bad params where they make sense

XFindFirstSaveGame()
        - test with 0 saved games
        - 1 saved game
        - a disk full of saved games (thousands!)
        - bad params where they make sense

XFindNextSaveGame()
        - test with 0 saved games
        - 1 saved game
        - a disk full of saved games (thousands!)
        - bad params where they make sense

XFindClose()
        - good handle
        - bad handle
        - double close
