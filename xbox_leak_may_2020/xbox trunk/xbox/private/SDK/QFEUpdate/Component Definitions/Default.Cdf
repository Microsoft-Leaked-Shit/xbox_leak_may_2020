[Info]
Name=
Type=CompDef
Version=2.10.000

[VC Components]
OBJECT=No
DESCRIPTION=
STATUS=MSG_COPY_UPDATE
VISIBLE=Yes
DISK=ANYDISK
FILENEED=STANDARD
INCLUDEINBUILD=Yes
PASSWORD=
ENCRYPT=No
COMPRESSIFSEPARATE=No
UNINSTALLABLE=Yes
COMMENT=
DEFSELECTION=Yes
SELECTED=Yes
IMAGE=
TARGETDIRCDROM=VCAdd-In
DISPLAYTEXT=
HTTPLOCATION=
FTPLOCATION=
MISC=
GUID=0a416371-6973-430e-a8a3-235ef14400f1
_SPLIT_BEFORE=
_SPLIT_AFTER=
_DATAASFILES=
_NO_SPLIT=
_NO_SPLIT_BEFORE=
VOLATILE=
filegroup0=VC Add-Ins
HANDLERONInstalling=
HANDLERONInstalled=
HANDLERONUnInstalling=
HANDLERONUnInstalled=

[Components]
component0=VC Components
component1=Documentation

[Documentation]
OBJECT=No
DESCRIPTION=This component includes the Xbox SDK documentation in the HTML Help format.
STATUS=MSG_COPY_UPDATE
VISIBLE=Yes
DISK=ANYDISK
FILENEED=HIGHLYRECOMMENDED
INCLUDEINBUILD=Yes
PASSWORD=
ENCRYPT=No
COMPRESSIFSEPARATE=No
UNINSTALLABLE=Yes
COMMENT=
DEFSELECTION=Yes
SELECTED=Yes
IMAGE=
TARGETDIRCDROM=Doc
DISPLAYTEXT=Xbox SDK Documentation
HTTPLOCATION=
FTPLOCATION=
MISC=
GUID=9124a12e-fb95-4251-a226-1fd7935c3d24
_SPLIT_BEFORE=
_SPLIT_AFTER=
_DATAASFILES=
_NO_SPLIT=
_NO_SPLIT_BEFORE=
VOLATILE=
filegroup0=Misc
filegroup1=Xbox Libraries
HANDLERONInstalling=
HANDLERONInstalled=
HANDLERONUnInstalling=
HANDLERONUnInstalled=

[TopComponents]
component0=Documentation
component1=VC Components

[SetupType]
setuptype0=Compact

[SetupTypeItem-Compact]
Comment=
Descrip=Installs all Xbox SDK files
DisplayText=Complete
item0=SDK Basics and Help
item1=SDK Tools
item2=Sample DirectX Applications
item3=SDK Libraries, Headers and Help Files
item4=Sample Code\SDK Examples
item5=DirectX Runtime
item6=Documentation\DirectX 6
item7=SDK Libraries and Header Files
item8='C' Samples
item9=Sample Code\Rockem 3D
item10=SDK Binaries
item11=Sample Code
item12=Sample Source Code\Immortal Klowns
item13=DirectX (tm)
item14=VC Components
item15=DirectX SDK
item16=SDK Binaries\Rockem Sample
item17=Sample Source Code
item18='C' Compile Environment
item19=Sample Code\Immortal Klowns
item20=Samples
item21=Sample Source Code\FoxBear
item22=Sample Binaries
item23=Sample Binaries\Rockem Sample
item24=SDK Tools\Rockem Sample
item25=Compile Environment
item26=Sample Source Code\Rockem 3D
item27=SDK Documentation
item28=Sample Code\FoxBear
item29=Documentation

