4/6/01 - All memory tests except performance and Memory Transfer, all CPU tests, all RTC tests, Hard Disk free space test, and all grafx tests.
Ran for 14 hours, and then failed due to XMTA errors trying to alloc space for CPU.FPU_STRESS a few times
(though other tests ran fine after and during these errors), and then finally stopped when the
Memory.WindowedCacheRandomDwordOps test failed to allocate 40000000000008h bytes of data (too much).
Grafx.VidMemory, CPU.FPU_STRESS, and HardDisk.Free_space_random_wrc were running at the time and all exited.
The FPU_STRESS test only failed while the grafx.shapes test was running.  The shapes test PASSED and then the
FPU_STRESS test ran successfully for several passes before the Memory test finally failed.

4/9/01 - All memory tests except performance and Memory Transfer; all MEmory tests at 4MB except WindowedCacheRandomDWORDOps,
which is at 50MB; all CPU tests, all RTC tests, Hard Disk free space test, and all grafx tests.
Ran for 9 hours, and then failed due to hang (no errors).  Only CPU.Internal_Speed and Grafx.3dsurface tests
were running at the time of the hang.

